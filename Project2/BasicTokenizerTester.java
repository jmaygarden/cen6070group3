import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * This class runs test cases for the BasicTokenizer class and reports the
 * results.
 */
public class BasicTokenizerTester {
	private static final String TEST[] = { "1.1", "1.2", "1.3", "1.4", "1.5",
			"1.6", "1.7", "1.8", "2.1", "2.2", "2.3", "2.4", "3.1", "3.2",
			"3.3", "3.4", "3.5", "3.6", "3.7", "4.1", "4.2", "4.3", "4.4",
			"4.5", "4.6", "4.7", "4.8", "4.9", "4.10", "4.11", "4.12", "4.13",
			"4.14", "4.15", "4.16", "4.17", "4.18", "4.19", "4.20", "4.21",
			"4.22", "4.23", "4.24", "4.25", "4.26", "4.27", "4.28", "4.29",
			"4.30", "4.31", "4.32", "4.33", "4.34", "4.35", "4.36", "4.37",
			"4.38", "4.39", "4.40", "4.41", "4.42", "4.43", "4.44", "4.45",
			"4.46", "4.47", "4.48", "4.49", "4.50", "4.51", "4.52", "4.53",
			"4.54", "4.55", "4.56", "4.57", "4.58", "4.59", "4.60", "4.61",
			"4.62", "4.63", "4.64", "4.65", "4.66", "4.67", "4.68", "4.69",
			"4.70", "4.71", "4.72", "4.73", "4.74", "4.75", "4.76", "4.77",
			"4.78", "4.79", "4.80", "4.81", "4.82", "4.83", "4.84", "4.85",
			"4.86", "4.87", "4.88", "4.89", "4.90", "4.91", "4.92", "4.93",
			"4.94", "4.95", "4.96", "5.1", "5.2", "5.3", "5.4", "5.5", "5.6",
			"5.7", "5.8", "5.9", "5.10", "5.11", "5.12", "5.13", "5.14",
			"5.15", "5.16", "5.17", "5.18", "5.19", "5.20", "5.21", "5.22",
			"5.23", "5.24", "5.25", "5.26", "5.27", "5.28", "5.29", "5.30",
			"5.31", "5.32", "5.33", "5.34", "5.35", "5.36", "5.37", "5.38",
			"5.39", "5.40", "5.41", "5.42", "5.43", "5.44", "5.45", "5.46",
			"5.47", "5.48", "6.1", "6.2", "6.3", "6.4", "6.5", "6.6", "6.7",
			"6.8", "6.9", "6.10", };
	private static final String S1 = "This\tis\ta\ttest";
	private static final String S2 = "This\nis\na\ntest";
	private static final String S3 = "This\ris\ra\rtest";
	private static final String S4 = "This\fis\fa\ftest";
	private static final String S5 = "This is a test";
	private static final String S6 = "Thisisatest";
	private static final String S7 = "\t";
	private static final String D[] = { "s", "ie", };
	private static final boolean R[] = { true, false };

	private int passed;
	private int total;
	private BasicTokenizer instance;

	public BasicTokenizerTester() {
		passed = 0;
		total = 0;
		instance = null;
	}

	/**
	 * Helper function for reporting a passing test
	 */
	private void pass() {
		System.out.printf("Test %s passed.\n", TEST[total]);
		passed += 1;
		total += 1;
	}

	/**
	 * Helper function for reporting a failing test
	 */
	private void fail() {
		System.out.printf("Test %s failed.\n", TEST[total]);
		total += 1;
	}

	/**
	 * Performs isolated testing of the first constructor
	 */
	private void testGroup1() {
		final String s[] = { S1, S2, S3, S4, S5, S6, S7 };

		/* test that valid strings are constructed without exception */
		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				pass();
			} catch (Exception e) {
				fail();
			}
		}

		/* ensure that a null string causes a NullPointer Exception */
		try {
			instance = new BasicTokenizer(null);
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}
	}

	/**
	 * Performs isolated testing of the second constructor
	 */
	private void testGroup2() {
		try {
			instance = new BasicTokenizer(null, D[0]);
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			pass();
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				pass();
			} catch (Exception e) {
				fail();
			}
		}
	}

	/**
	 * Performs isolated testing of the third constructor
	 */
	private void testGroup3() {
		try {
			instance = new BasicTokenizer(null, D[0], true);
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < R.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, null, R[i]);
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int j = 0; j < D.length; ++j) {
				try {
					instance = new BasicTokenizer(S5, D[j], R[i]);
					pass();
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the countTokens() method
	 */
	private void testCountTokensIsolated() {
		final String s[] = { S1, S2, S3, S4, S5 };

		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				if (4 == instance.countTokens()) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		try {
			instance = new BasicTokenizer(S6);
			if (1 == instance.countTokens()) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S7);
			if (0 == instance.countTokens()) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			instance.countTokens();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				if (4 == instance.countTokens()) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		final int count[] = { 7, 4 };

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				instance.countTokens();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					if (count[j] == instance.countTokens()) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the hasMoreElements() method
	 */
	private void testHasMoreElementsIsolated() {
		String s[] = { S1, S2, S3, S4, S5, S6 };

		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				if (true == instance.hasMoreElements()) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		try {
			instance = new BasicTokenizer(S7);
			if (true == instance.hasMoreElements()) {
				fail();
			} else {
				pass();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			instance.hasMoreElements();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				if (true == instance.hasMoreElements()) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				instance.hasMoreElements();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					if (true == instance.hasMoreElements()) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the hasMoreTokens() method
	 */
	private void testHasMoreTokensIsolated() {
		String s[] = { S1, S2, S3, S4, S5, S6 };

		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				if (true == instance.hasMoreTokens()) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		try {
			instance = new BasicTokenizer(S7);
			if (true == instance.hasMoreTokens()) {
				fail();
			} else {
				pass();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			instance.hasMoreTokens();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				if (true == instance.hasMoreTokens()) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				instance.hasMoreTokens();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					if (true == instance.hasMoreTokens()) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the nextElement() method
	 */
	private void testNextElementIsolated() {
		String s[] = { S1, S2, S3, S4, S5 };

		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				if (instance.nextElement().equals("This")) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		try {
			instance = new BasicTokenizer(S6);
			if (instance.nextElement().equals("Thisisatest")) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S7);
			instance.nextElement();
			fail();
		} catch (NoSuchElementException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			instance.nextElement();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		final String output[] = { "Thi", "Th" };

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				if (instance.nextElement().equals(output[i])) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				instance.nextElement();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					if (instance.nextElement().equals(output[i])) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the nextToken() method
	 */
	private void testNextToken1Isolated() {
		String s[] = { S1, S2, S3, S4, S5 };

		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				if (instance.nextToken().equals("This")) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		try {
			instance = new BasicTokenizer(S6);
			if (instance.nextToken().equals("Thisisatest")) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S7);
			instance.nextToken();
			fail();
		} catch (NoSuchElementException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			instance.nextToken();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		final String output[] = { "Thi", "Th" };

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				if (instance.nextToken().equals(output[i])) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				instance.nextToken();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					if (instance.nextToken().equals(output[i])) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the nextToken(String) method
	 */
	private void testNextToken2Isolated() {
		String s[] = { S1, S2, S3, S4, S5, S6 };

		for (int i = 0; i < s.length; ++i) {
			try {
				instance = new BasicTokenizer(s[i]);
				if (instance.nextToken("h").equals("T")) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		try {
			instance = new BasicTokenizer(S7);
			if (instance.nextToken("h").equals("\t")) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			if (instance.nextToken("h").equals("T")) {
				pass();
			} else {
				fail();
			}
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				if (instance.nextToken("h").equals("T")) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				if (instance.nextToken("h").equals("T")) {
					pass();
				} else {
					fail();
				}
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					if (instance.nextToken("h").equals("T")) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs isolated testing of the pubic class methods
	 */
	private void testGroup4() {
		testCountTokensIsolated();
		testHasMoreElementsIsolated();
		testHasMoreTokensIsolated();
		testNextElementIsolated();
		testNextToken1Isolated();
		testNextToken2Isolated();
	}

	private List<Object> executePatternC1() {
		List<Object> result = new ArrayList<Object>();

		result.add(instance.nextToken());
		result.add(instance.countTokens());
		result.add(instance.hasMoreElements());

		return result;
	}

	private List<Object> executePatternC2() {
		List<Object> result = new ArrayList<Object>();

		result.add(instance.nextToken("h"));
		result.add(instance.countTokens());
		result.add(instance.hasMoreElements());

		return result;
	}

	private List<Object> executePatternC3() {
		List<Object> result = new ArrayList<Object>();

		result.add(instance.nextElement());
		result.add(instance.countTokens());
		result.add(instance.hasMoreElements());

		return result;
	}

	private void testPatternC1() {
		List<Object> expected = new ArrayList<Object>();
		List<Object> result;

		{
			String s[] = { S1, S2, S3, S4, S5 };

			expected.clear();
			expected.add("This");
			expected.add(3);
			expected.add(true);

			for (int i = 0; i < s.length; ++i) {
				try {
					instance = new BasicTokenizer(s[i]);
					result = executePatternC1();
					if (result.equals(expected)) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}

		expected.clear();
		expected.add("Thisisatest");
		expected.add(0);
		expected.add(false);

		try {
			instance = new BasicTokenizer(S6);
			result = executePatternC1();
			if (result.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S7);
			result = executePatternC1();
			fail();
		} catch (NoSuchElementException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			result = executePatternC1();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		expected.clear();
		expected.add("This");
		expected.add(3);
		expected.add(true);

		for (int i = 0; i < D.length; ++i) {
			expected.set(0, (0 == i) ? "Thi" : "Th");

			try {
				instance = new BasicTokenizer(S5, D[i]);
				result = executePatternC1();
				if (result.equals(expected)) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		expected.clear();
		expected.add("Thi");
		expected.add(6);
		expected.add(true);

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				result = executePatternC1();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			expected.set(1, (0 == j) ? 6 : 3);

			for (int i = 0; i < D.length; ++i) {
				expected.set(0, (0 == i) ? "Thi" : "Th");

				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					result = executePatternC1();
					if (result.equals(expected)) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	private void testPatternC2() {
		List<Object> expected = new ArrayList<Object>();
		List<Object> result;

		{
			String s[] = { S1, S2, S3, S4, S5, S6 };

			expected.clear();
			expected.add("T");
			expected.add(1);
			expected.add(true);

			for (int i = 0; i < s.length; ++i) {
				try {
					instance = new BasicTokenizer(s[i]);
					result = executePatternC2();
					if (result.equals(expected)) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}

		expected.clear();
		expected.add("\t");
		expected.add(0);
		expected.add(false);

		try {
			instance = new BasicTokenizer(S7);
			result = executePatternC2();
			if (result.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		expected.clear();
		expected.add("T");
		expected.add(1);
		expected.add(true);

		try {
			instance = new BasicTokenizer(S5, null);
			result = executePatternC2();
			if (result.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		for (int i = 0; i < D.length; ++i) {
			try {
				instance = new BasicTokenizer(S5, D[i]);
				result = executePatternC2();
				if (result.equals(expected)) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		for (int j = 0; j < R.length; ++j) {
			expected.set(1, (0 == j) ? 2 : 1);

			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				result = executePatternC2();
				if (result.equals(expected)) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}

			for (int i = 0; i < D.length; ++i) {
				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					result = executePatternC2();
					if (result.equals(expected)) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	private void testPatternC3() {
		List<Object> expected = new ArrayList<Object>();
		List<Object> result;

		{
			String s[] = { S1, S2, S3, S4, S5 };

			expected.clear();
			expected.add("This");
			expected.add(3);
			expected.add(true);

			for (int i = 0; i < s.length; ++i) {
				try {
					instance = new BasicTokenizer(s[i]);
					result = executePatternC3();
					if (result.equals(expected)) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}

		expected.clear();
		expected.add("Thisisatest");
		expected.add(0);
		expected.add(false);

		try {
			instance = new BasicTokenizer(S6);
			result = executePatternC3();
			if (result.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S7);
			result = executePatternC3();
			fail();
		} catch (NoSuchElementException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		try {
			instance = new BasicTokenizer(S5, null);
			result = executePatternC3();
			fail();
		} catch (NullPointerException e) {
			pass();
		} catch (Exception e) {
			fail();
		}

		expected.clear();
		expected.add("Thi");
		expected.add(3);
		expected.add(true);

		for (int i = 0; i < D.length; ++i) {
			expected.set(0, (0 == i) ? "Thi" : "Th");
			try {
				instance = new BasicTokenizer(S5, D[i]);
				result = executePatternC3();
				if (result.equals(expected)) {
					pass();
				} else {
					fail();
				}
			} catch (Exception e) {
				fail();
			}
		}

		expected.clear();
		expected.add("Thi");
		expected.add(6);
		expected.add(true);

		for (int j = 0; j < R.length; ++j) {
			try {
				instance = new BasicTokenizer(S5, null, R[j]);
				result = executePatternC3();
				fail();
			} catch (NullPointerException e) {
				pass();
			} catch (Exception e) {
				fail();
			}

			expected.set(1, (0 == j) ? 6 : 3);

			for (int i = 0; i < D.length; ++i) {
				expected.set(0, (0 == i) ? "Thi" : "Th");

				try {
					instance = new BasicTokenizer(S5, D[i], R[j]);
					result = executePatternC3();
					if (result.equals(expected)) {
						pass();
					} else {
						fail();
					}
				} catch (Exception e) {
					fail();
				}
			}
		}
	}

	/**
	 * Performs testing of public method call patterns
	 */
	private void testGroup5() {
		testPatternC1();
		testPatternC2();
		testPatternC3();
	}

	/**
	 * Performs fuzz testing
	 */
	private void testGroup6() {
		final String s1 = "This is a test with special values.";
		final String s2 = "\nThis is a test\n";
		final String s3 = "\u0001\u0002\u0003\u0004";
		final String s4 = "This\0 This";
		final String s5 = "\u0129\u0130\u0131";
		final String d1 = "aeiou";
		final String d2 = "This ";
		final String d3 = "\n";
		final String d4 = "\u0002\u0004";
		final String d5 = "\u0130";
		List<Object> expected = new ArrayList<Object>();
		List<Object> actual = new ArrayList<Object>();

		// 6.1
		instance = new BasicTokenizer(s1, d1, false);
		expected.clear();
		expected.add("Th");
		expected.add("s ");
		expected.add("s ");
		expected.add(" t");
		expected.add("st w");
		expected.add("th sp");
		expected.add("c");
		expected.add("l v");
		expected.add("l");
		expected.add("s.");
		actual.clear();
		try {
			while (instance.hasMoreTokens()) {
				String token = instance.nextToken();
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.2
		instance = new BasicTokenizer(s1);
		expected.clear();
		expected.add("This is ");
		expected.add("a t");
		expected.add("est w");
		expected.add("ith special values.");
		actual.clear();
		try {
			int counter = 0;

			while (instance.hasMoreTokens()) {
				int index = counter++ % d1.length();
				String token = instance.nextToken(d1
						.substring(index, index + 1));
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.3
		instance = new BasicTokenizer(s1, d2, false);
		expected.clear();
		expected.add("a");
		expected.add("te");
		expected.add("t");
		expected.add("w");
		expected.add("t");
		expected.add("pec");
		expected.add("al");
		expected.add("value");
		expected.add(".");
		actual.clear();
		try {
			while (instance.hasMoreTokens()) {
				String token = instance.nextToken();
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.4
		instance = new BasicTokenizer(s2, d3, false);
		expected.clear();
		expected.add("This is a test");
		actual.clear();
		try {
			while (instance.hasMoreTokens()) {
				String token = instance.nextToken();
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.5
		instance = new BasicTokenizer(s1);
		expected.clear();
		expected.add(7);
		expected.add(true);
		expected.add(true);
		expected.add(7);
		expected.add(true);
		expected.add(true);
		actual.clear();
		try {
			for (int i = 0; i < 2; ++i) {
				actual.add(instance.countTokens());
				actual.add(instance.hasMoreElements());
				actual.add(instance.hasMoreTokens());
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.6
		instance = new BasicTokenizer(s3, d4);
		expected.clear();
		expected.add("\u0001");
		expected.add("\u0003");
		actual.clear();
		try {
			while (instance.hasMoreTokens()) {
				String token = instance.nextToken();
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.7
		instance = new BasicTokenizer(s4);
		expected.clear();
		expected.add("This\0");
		expected.add("This");
		actual.clear();
		try {
			while (instance.hasMoreTokens()) {
				String token = instance.nextToken();
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}

		// 6.8
		instance = new BasicTokenizer(s5, d5);
		expected.clear();
		expected.add("\u0129");
		expected.add("\u0131");
		actual.clear();
		try {
			while (instance.hasMoreTokens()) {
				String token = instance.nextToken();
				actual.add(token);
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}
		
		// 6.9
		instance = new BasicTokenizer(s1);
		expected.clear();
		expected.add("This");
		expected.add("is");
		expected.add("a");
		expected.add("test");
		expected.add("with");
		expected.add("special");
		expected.add("values.");
		actual.clear();
		try {
			Enumeration<Object> e = instance;
			while (e.hasMoreElements()) {
				actual.add(e.nextElement());
			}
			if (actual.equals(expected)) {
				pass();
			} else {
				fail();
			}
		} catch (Exception e) {
			fail();
		}
	}

	/**
	 * Class unit test driver, is static for easy access from main(). Returns
	 * true if all tests passed, false if some test failed.
	 */
	public void run() {
		testGroup1();
		testGroup2();
		testGroup3();
		testGroup4();
		testGroup5();
		testGroup6();

		System.out.printf("%d of %d tests passed\n", passed, total);
	}

	/**
	 * main() is a test driver
	 */
	public static void main(String args[]) {
		BasicTokenizerTester tester = new BasicTokenizerTester();
		tester.run();
	}
}
